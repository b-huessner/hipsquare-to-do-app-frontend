import { ChangeEvent } from "react";
import styled from "styled-components";
import { StatusFilter } from "../types/StatusFilter";

const Wrapper = styled.div`
  margin-bottom: 24px;
`;

const FilterSelect = styled.select`
  padding: 6px;
  border-radius: 8px;
  border: 1px solid #777;
  margin-right: 12px;
`;

const options: ToDoListFilterOptions[] = [
  { value: StatusFilter.All, label: "Alle" },
  { value: StatusFilter.Done, label: "Erledigt" },
  { value: StatusFilter.Open, label: "Nicht erledigt" },
];

interface ToDoListFilterOptions {
  value: string;
  label: string;
}

export interface ToDoListFilterProps {
  setFilterValue: (value: StatusFilter) => void;
}

export default function ToDoListFilter({
  setFilterValue,
}: ToDoListFilterProps) {
  function onChange(event: ChangeEvent<HTMLSelectElement>) {
    const target = event.target as HTMLSelectElement;
    const value = target.value as StatusFilter;

    setFilterValue(value);
  }

  return (
    <Wrapper>
      <FilterSelect onChange={onChange}>
        {options.map((option) => (
          <option key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </FilterSelect>
    </Wrapper>
  );
}
