import {
  fireEvent,
  getByText,
  render,
  RenderResult,
} from "@testing-library/react";
import ToDoEntry from "./ToDoEntry";
import { ToDo } from "../types/ToDo";

describe("ToDoEntry", () => {
  let toDo: ToDo;
  let renderResult: RenderResult;
  let checkbox: HTMLInputElement;
  let mockDoneFunction: (id: number, done: boolean) => void;

  beforeEach(() => {
    toDo = {
      id: 0,
      Name: "To do 1",
      Done: false,
    };
    mockDoneFunction = jest.fn();
    renderResult = render(
      <ToDoEntry toDo={toDo} updateToDoDoneStatus={mockDoneFunction} />
    );
    checkbox = renderResult.container.querySelector(
      "input[type=checkbox]"
    ) as HTMLInputElement;
  });

  it("should render a to-do", () => {
    const { getByText, container } = renderResult;
    expect(getByText(toDo.Name ?? "")).toBeInTheDocument();
    expect(container.querySelector("input[type=checkbox]")).toBeTruthy();
  });

  it("should have the checkbox un-checked for a to-do that is not done", () => {
    const { container } = renderResult;

    expect(checkbox.checked).toEqual(false);
  });

  it("should have the checkbox checked for a to-do that is done", () => {
    renderResult = render(
      <ToDoEntry
        toDo={{ ...toDo, Done: true }}
        updateToDoDoneStatus={mockDoneFunction}
      />
    );
    checkbox = renderResult.container.querySelector(
      "input[type=checkbox]"
    ) as HTMLInputElement;
    expect(checkbox.checked).toEqual(true);
  });

  it("should call the updateToDoDoneStatus function when clicking the checkbox and the to do is not done", () => {
    fireEvent.click(checkbox);
    expect(mockDoneFunction).toHaveBeenCalledWith(toDo.id, true);
  });

  it("should call the updateToDoDoneStatus function when clicking the checkbox and the to do is done", () => {
    renderResult = render(
      <ToDoEntry
        toDo={{ ...toDo, Done: true }}
        updateToDoDoneStatus={mockDoneFunction}
      />
    );
    checkbox = renderResult.container.querySelector(
      "input[type=checkbox]"
    ) as HTMLInputElement;
    fireEvent.click(checkbox);
    expect(mockDoneFunction).toHaveBeenLastCalledWith(toDo.id, false);
  });
});
