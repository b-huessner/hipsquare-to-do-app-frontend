import { StatusFilter } from "../types/StatusFilter";
import { NewToDoDto, ToDo } from "../types/ToDo";
import { NewToDoForm } from "./NewToDoForm";
import ToDoEntry from "./ToDoEntry";
import ToDoListFilter from "./ToDoListFilter";

export interface ToDoListProps {
  toDos: ToDo[];
  updateToDoDoneStatus: (id: number, done: boolean) => void;
  addNewToDo: (toDoToCreate: NewToDoDto) => void;
  setFilterValue: (value: StatusFilter) => void;
}

export function ToDoList({
  toDos,
  updateToDoDoneStatus,
  addNewToDo,
  setFilterValue,
}: ToDoListProps): JSX.Element {
  return (
    <>
      <h1>To-Dos</h1>
      <NewToDoForm addNewToDo={addNewToDo} />
      <ToDoListFilter setFilterValue={setFilterValue} />
      {toDos
        .sort((t1, t2) => +(t1.Done || false) - +(t2.Done || false))
        .map((toDo) => (
          <ToDoEntry
            key={toDo.id}
            toDo={toDo}
            updateToDoDoneStatus={updateToDoDoneStatus}
          />
        ))}
    </>
  );
}
