import { render, RenderResult } from "@testing-library/react";
import { ToDoList } from "./ToDoList";
import { mockToDos } from "../lib-test/mock-todos";

describe("ToDoList", () => {
  let renderResult: RenderResult;
  let updateToDoDoneStatusMock = jest.fn();

  beforeEach(() => {
    renderResult = render(
      <ToDoList
        toDos={mockToDos}
        updateToDoDoneStatus={updateToDoDoneStatusMock}
        addNewToDo={() => {}}
        setFilterValue={() => {}}
      />
    );
  });

  it("should render all to-dos", () => {
    expect(renderResult.container).toBeTruthy();
    for (const toDo of mockToDos) {
      expect(renderResult.getByText(toDo.Name ?? "")).toBeInTheDocument();
    }
  });
});
