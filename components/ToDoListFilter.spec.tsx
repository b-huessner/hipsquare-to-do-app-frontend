import { fireEvent, render, RenderResult } from "@testing-library/react";
import { mockToDos } from "../lib-test/mock-todos";
import { StatusFilter } from "../types/StatusFilter";
import { ToDoList } from "./ToDoList";

describe("ToDoListFilter", () => {
  let renderResult: RenderResult;
  let setFilterValueMock = jest.fn();
  let updateToDoDoneStatusMock = jest.fn();
  let addNewToDoMock = jest.fn();
  let select: HTMLSelectElement;

  beforeEach(() => {
    renderResult = render(
      <ToDoList
        toDos={mockToDos}
        setFilterValue={setFilterValueMock}
        updateToDoDoneStatus={updateToDoDoneStatusMock}
        addNewToDo={addNewToDoMock}
      />
    );
    select = renderResult.container.querySelector(
      "select"
    ) as HTMLSelectElement;
  });

  afterEach(() => {
    setFilterValueMock.mockClear();
    updateToDoDoneStatusMock.mockClear();
    addNewToDoMock.mockClear();
    renderResult.unmount();
  });

  it("should render the filter select with the correct options", () => {
    const container = renderResult.container;
    expect(container).toBeVisible();
    expect(container.querySelector("select")).toBeVisible();
    expect(container.querySelectorAll("option")).toHaveLength(3);
    [StatusFilter.All, StatusFilter.Open, StatusFilter.Done].forEach(
      (value) => {
        expect(container.querySelector(`option[value=${value}]`)).toBeVisible();
      }
    );
  });

  it("has the all filter selected by default", () => {
    expect(select.value).toEqual(StatusFilter.All);
  });

  it("should call 'setFilterValue' with the selected value", () => {
    [StatusFilter.All, StatusFilter.Done, StatusFilter.Open].forEach(
      (value) => {
        fireEvent.change(select, {
          target: { value },
        });
        expect(setFilterValueMock).toHaveBeenCalledWith(value);
      }
    );

    expect(setFilterValueMock).toHaveBeenCalledTimes(3);
  });

  it("should render all todos when filter 'All' is selected", () => {
    mockToDos.forEach((todo) =>
      expect(renderResult.getByText(todo.Name ?? ""))
    );
  });

  it("should render all todos when filter 'Done' is selected", () => {
    fireEvent.change(select, { target: { value: StatusFilter.Done } });

    mockToDos
      .filter((todo) => todo.Done)
      .forEach((todo) => expect(renderResult.getByText(todo.Name ?? "")));
  });

  it("should render all todos when filter 'Open' is selected", () => {
    fireEvent.change(select, { target: { value: StatusFilter.Open } });

    mockToDos
      .filter((todo) => !todo.Done)
      .forEach((todo) => expect(renderResult.getByText(todo.Name ?? "")));
  });
});
