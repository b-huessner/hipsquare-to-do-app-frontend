import { ChangeEvent, useState } from "react";
import styled from "styled-components";
import { NewToDoDto } from "../types/ToDo";

const Wrapper = styled.div`
  display: flex;
  margin-bottom: 24px;
`;

const NewToDoInput = styled.input`
  padding: 6px;
  border-radius: 8px;
  border: 1px solid #777;
  margin-right: 12px;
`;

const SubmitButton = styled.button`
  padding: 6px 12px;
  border-radius: 8px;
  border: 1px solid #777;
`;

export interface NewToDoFormProps {
  addNewToDo: (toDoToCreate: NewToDoDto) => void;
}

export function NewToDoForm({ addNewToDo }: NewToDoFormProps) {
  const [toDoName, setToDoName] = useState("");

  const onChangeToDoName = (event: ChangeEvent) => {
    const target = event.target as HTMLInputElement;
    const value = target.value;

    setToDoName(value);
  };

  const onSubmit = (event: React.FormEvent) => {
    const toDoToCreate: NewToDoDto = {
      Name: toDoName.trim(),
      Done: false,
    };
    event.preventDefault();

    if (toDoToCreate.Name && toDoToCreate.Name.length > 0) {
      addNewToDo(toDoToCreate);
      setToDoName("");
    }
  };

  return (
    <Wrapper>
      <form onSubmit={onSubmit}>
        <NewToDoInput
          type="text"
          value={toDoName}
          onChange={onChangeToDoName}
        />
        <SubmitButton disabled={toDoName.trim().length === 0} type="submit">
          Hinzufügen
        </SubmitButton>
      </form>
    </Wrapper>
  );
}
