import { fireEvent, render, RenderResult } from "@testing-library/react";
import { NewToDoForm } from "./NewToDoForm";

describe("NewToDoForm", () => {
  let renderResult: RenderResult;
  let addNewToDoMock: jest.Mock<any, any>;
  let nameInput: HTMLInputElement;
  let submitButton: HTMLInputElement;

  beforeEach(() => {
    addNewToDoMock = jest.fn();
    renderResult = render(<NewToDoForm addNewToDo={addNewToDoMock} />);

    nameInput = renderResult.container.querySelector(
      "input[type=text]"
    ) as HTMLInputElement;
    submitButton = renderResult.container.querySelector(
      "button[type=submit]"
    ) as HTMLInputElement;
  });

  afterEach(() => {
    addNewToDoMock.mockClear();
    renderResult.unmount();
  });

  it("should render the name input and the submit button", () => {
    expect(renderResult.container).toBeVisible();
    expect(nameInput).toBeVisible();
    expect(submitButton).toBeVisible();
    expect(submitButton).toBeDisabled();
  });

  it("should add new ToDo with a name supplied", () => {
    fireEvent.change(nameInput, { target: { value: "Foo" } });
    fireEvent.submit(nameInput);

    expect(addNewToDoMock).toHaveBeenNthCalledWith(1, {
      Name: "Foo",
      Done: false,
    });

    fireEvent.change(nameInput, { target: { value: "Bar" } });
    fireEvent.submit(nameInput);

    expect(addNewToDoMock).toHaveBeenNthCalledWith(2, {
      Name: "Bar",
      Done: false,
    });
  });

  it("should not add new ToDo without a name supplied", () => {
    fireEvent.change(nameInput, { target: { value: "" } });

    // Test submitting via both input and button
    fireEvent.submit(nameInput);
    fireEvent.click(nameInput);

    expect(addNewToDoMock).toHaveBeenCalledTimes(0);
  });

  it("should trim whitespacees before creating a new todo", () => {
    fireEvent.change(nameInput, { target: { value: "    Foo    " } });
    fireEvent.submit(nameInput);

    expect(addNewToDoMock).toHaveBeenCalledWith({ Name: "Foo", Done: false });
  });

  it("should disable the submit button when no valid value is in the input field", () => {
    fireEvent.change(nameInput, { target: { value: " " } });

    expect(submitButton).toBeDisabled();

    fireEvent.change(nameInput, { target: { value: " Foo" } });

    expect(submitButton).toBeEnabled();
  });
});
