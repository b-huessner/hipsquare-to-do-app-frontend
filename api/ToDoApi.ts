import { NewToDoDto, ToDo } from "../types/ToDo";

export interface ToDoApi {
  getAllToDos(): Promise<ToDo[]>;
  updateToDo(id: number, toDo: ToDo): Promise<void>;
  addToDo(todo: NewToDoDto): Promise<ToDo>;
}
