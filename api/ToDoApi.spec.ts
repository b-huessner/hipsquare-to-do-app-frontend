import { ToDoApi } from "./ToDoApi";
import { mockToDoResponse, mockToDos } from "../lib-test/mock-todos";
import { ToDoApiImpl } from "./ToDoApiImpl";
import { NewToDoDto, ToDo } from "../types/ToDo";

describe("To Do API", () => {
  let toDoApi: ToDoApi;
  let backendUrl = "http://backend";
  let toDoEndpoint = "/to-dos";

  beforeEach(() => (toDoApi = new ToDoApiImpl(backendUrl, toDoEndpoint)));

  it("should fetch all to-dos from an HTTP endpoint", async () => {
    // Setup
    const fetchMock = jest.fn().mockReturnValue(
      Promise.resolve({
        json: () => Promise.resolve(mockToDos),
      }) as Promise<Response>
    );
    global.fetch = fetchMock;

    // Test
    expect(await toDoApi.getAllToDos()).toEqual(mockToDos);
    expect(fetchMock).toHaveBeenCalledWith(`${backendUrl}${toDoEndpoint}`);

    // Cleanup
    fetchMock.mockClear();
  });

  it("should update a to-do using the HTTP endpoint", async () => {
    // Setup
    const fetchMock = jest
      .fn()
      .mockReturnValue(Promise.resolve({ json: () => Promise.resolve() }));
    global.fetch = fetchMock;

    const updatedToDo = { id: 0, Name: "new name", Done: false };

    // Test
    await toDoApi.updateToDo(updatedToDo.id, updatedToDo);
    expect(fetchMock).toHaveBeenCalledWith(
      `${backendUrl}${toDoEndpoint}/${updatedToDo.id}`,
      {
        body: JSON.stringify(updatedToDo),
        headers: {
          "Content-Type": "application/json",
        },
        method: "PUT",
      }
    );

    // Cleanup
    fetchMock.mockClear();
  });

  it("should create a to-do using the HTTP endpoint", async () => {
    // Setup
    const toDoToCreate: NewToDoDto = {
      Name: "Created ToDo",
      Done: false,
    };
    const fetchMock = jest
      .fn()
      .mockReturnValue(
        Promise.resolve({ json: () => Promise.resolve(mockToDoResponse) })
      );
    global.fetch = fetchMock;

    // Test
    const createdToDo = await toDoApi.addToDo(toDoToCreate);
    expect(createdToDo).toEqual(mockToDoResponse);
    expect(fetchMock).toHaveBeenCalledWith(`${backendUrl}${toDoEndpoint}`, {
      method: "POST",
      body: JSON.stringify(toDoToCreate),
      headers: {
        "Content-Type": "application/json",
      },
    });

    // Cleanup
    fetchMock.mockClear();
  });
});
