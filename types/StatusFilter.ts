export enum StatusFilter {
  All = "all",
  Open = "open",
  Done = "done",
}
