export interface ToDo {
  id: number;
  Name?: string;
  Done?: boolean;
}

export type NewToDoDto = Pick<ToDo, "Name" | "Done">;
