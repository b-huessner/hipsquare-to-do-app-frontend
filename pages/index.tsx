import type { GetServerSidePropsResult, NextPage } from "next";
import { useEffect, useState } from "react";
import { ToDoList } from "../components/ToDoList";
import { NewToDoDto, ToDo } from "../types/ToDo";
import { ToDoApiFactory } from "../api/ToDoApiFactory";
import { StatusFilter } from "../types/StatusFilter";

const toDoApi = ToDoApiFactory.get();

interface HomepageProps {
  toDos: ToDo[];
}

export async function getServerSideProps(): Promise<
  GetServerSidePropsResult<HomepageProps>
> {
  return {
    props: {
      toDos: await toDoApi.getAllToDos(),
    },
  };
}

const Home: NextPage<HomepageProps> = ({
  toDos: toDosFromProps,
}: HomepageProps) => {
  const [toDos, setToDos] = useState<ToDo[]>(toDosFromProps);
  const [filteredToDos, setFilteredToDos] = useState<ToDo[]>(toDos);
  const [filterValue, setFilterValue] = useState<StatusFilter>(
    StatusFilter.All
  );

  useEffect(() => {
    if (filterValue === StatusFilter.All) {
      setFilteredToDos(toDos);
      return;
    }

    const newFilteredToDos = toDos.filter((toDo) => {
      if (filterValue === StatusFilter.Done) {
        return toDo.Done === true;
      }
      if (filterValue === StatusFilter.Open) {
        return !toDo.Done;
      }
    });

    setFilteredToDos(newFilteredToDos);
  }, [filterValue, toDos]);

  async function addNewToDo(toDoToCreate: NewToDoDto) {
    let newToDo;

    try {
      newToDo = await toDoApi.addToDo(toDoToCreate);
    } catch (err) {
      console.error("An error occurred during creation of ToDo", err);
    }

    if (!newToDo) {
      return;
    }

    setToDos([...toDos, newToDo]);
  }

  async function updateToDoDoneStatus(
    id: number,
    done: boolean
  ): Promise<void> {
    const toDo: ToDo | undefined = toDos.find((toDo) => toDo.id === id);
    if (!toDo) {
      return;
    }

    const updatedToDo: ToDo = {
      ...toDo,
      Done: done,
    };

    setToDos((toDos) => [
      ...toDos.map((toDo) =>
        toDo.id === id && updatedToDo ? updatedToDo : toDo
      ),
    ]);
    await toDoApi.updateToDo(id, updatedToDo);
  }

  return (
    <ToDoList
      toDos={filteredToDos}
      addNewToDo={addNewToDo}
      updateToDoDoneStatus={updateToDoDoneStatus}
      setFilterValue={(value: StatusFilter) => setFilterValue(value)}
    />
  );
};

export default Home;
